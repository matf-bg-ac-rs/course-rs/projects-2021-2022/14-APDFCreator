# Project APDFCreator

APDFCreator (skraćeno od Academic PDF Creator) predstavlja program koji omogućava pomoću drag-and-drop funkcionalnosti, po principu WYSIWYG (what you see is what you get) kreiranje PDF fajlova. Ideja je da se ovaj program koristi kao sredstvo za lakše i brže kreiranje skripti za predavanja/vežbe kao i da omogući jednostavan način za studente da kreiraju svoj prvi CV.

## Dependencies

This app requires **latex** and **dvipng** packages to be installed on the system. 

You can set up LaTeX support on your Ubuntu installation by running:

`sudo apt install texlive-full`

On Windows, you can download and install the free [MiKTeX](https://miktex.org/) TeX distribution.

Make sure to add **latex** and **dvipng** packages to your _PATH_ environment variable.

### Note:

You will need to install the packages required by LaTeX upon first starting the APDFCreator.  

## Binaries

You can find the compiled binaries for Windows on the [Releases](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/14-APDFCreator/-/releases) page.

## Building

Download and install Qt 6.2.1 library (along with Qt Creator)

Position into the desired directory and clone the repository.

Open the project by opening the `CMakeLists.txt` file, and click on **Configure project**.

Change the build mode to **Release** and start building using the **Build->Build Project "APDF Creator"**

Move the built .exe file from build folder to a new one and run _pathToYourQtDirectory/windeployqt.exe pathToAPDFCreatorExe/APDFCreator.exe_

Copy **libstdc++-6.dll**, **libgcc_s_seh-1.dll** and **libwinpthread-1.dll** alongside APDFCreator.exe.

Building process completed.

## Developers

- [Predrag Draganović, 216/2017](https://gitlab.com/predragdraganovic)
- ~~[Aleksa Simić, 50/2017](https://gitlab.com/cikanone)~~ odustao
- [Nevena Mesar, 107/2015](https://gitlab.com/nevena-m)
- [Ivana Jovičić, 407/2021](https://gitlab.com/Anavi93)
